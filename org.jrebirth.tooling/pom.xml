<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.jrebirth</groupId>
		<artifactId>organization</artifactId>
		<version>2.0.1</version>
	</parent>

	<groupId>org.jrebirth</groupId>
	<artifactId>tooling</artifactId>
	<version>1.0.0-SNAPSHOT</version>
	<packaging>pom</packaging>

	<name>JRebirth Tools</name>
	<description>Set of tool modules around JRebirth Application Framework</description>
	<url>http://www.jrebirth.org</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<modules>
		<module>codegen</module>
		<module>jrebirth-maven-plugin</module>
		<module>ecore2fx</module>
	</modules>

	<issueManagement>
		<system>GitHub Issue Tracker</system>
		<url>http://bugs.jrebirth.org</url>
	</issueManagement>

	<ciManagement>
		<system>Jenkins</system>
		<url>http://ci.jrebirth.org/job/JRebirthTooling/</url>
	</ciManagement>

	<scm>
		<connection>scm:git:git://github.com/JRebirth/JRebirth-Tooling.git</connection>
		<developerConnection>scm:git:ssh://git@github.com/JRebirth/JRebirth-Tooling.git</developerConnection>
		<url>https://github.com/JRebirth/JRebirth-Tooling</url>
	</scm>

	<licenses>
		<license>
			<name>Apache License</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.html</url>
			<distribution>repo</distribution>
			<comments>Version 2.0, January 2004</comments>
		</license>
	</licenses>

	<developers>
		<developer>
			<id>jr1</id>
			<name>Bordes Sébastien</name>
			<email>seb@jrebirth.org</email>
			<url>blog.jrebirth.org</url>
			<organization>JRebirth</organization>
			<organizationUrl>www.jrebirth.org</organizationUrl>
			<roles>
				<role>JRebirth Designer</role>
			</roles>
		</developer>
	</developers>

	<build>

		<!-- Still not working 05/10/2011 NOTE: This is just a vision for the future, it's not yet implemented: see MNG-2216 -->
		<!--<sourceEncoding>UTF-8</sourceEncoding> -->

		<plugins>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<version>2.7</version>

				<dependencies>
					<dependency>
						<groupId>org.apache.maven.shared</groupId>
						<artifactId>maven-shared-jar</artifactId>
						<version>1.1</version>
						<exclusions>
							<exclusion>
								<groupId>org.apache.bcel</groupId>
								<artifactId>bcel</artifactId>
							</exclusion>
						</exclusions>
					</dependency>
					<dependency>
						<groupId>com.google.code.findbugs</groupId>
						<artifactId>bcel-findbugs</artifactId>
						<version>6.0</version>
					</dependency>
				</dependencies>

			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<version>3.5.1</version>

				<executions>
					<!-- <execution> <id>attach-sites</id> <goals> <goal>jar</goal> </goals> </execution> -->
					<execution>
						<id>build-sites</id>
						<phase>site</phase>
						<goals>
							<goal>site</goal>
							<goal>jar</goal>
							<goal>stage</goal>
						</goals>
					</execution>
					<!-- <execution> <id>package-sites</id> <phase>package</phase> <goals> <goal>jar</goal> </goals> </execution> -->
					<execution>
						<id>deploy-sites</id>
						<phase>site-deploy</phase>
						<goals>
							<goal>stage-deploy</goal>
						</goals>
					</execution>
				</executions>

				<configuration>
					<stagingDirectory>${project.build.directory}/staging-site</stagingDirectory>
					<stagingSiteURL>${project.distributionManagement.site.url}</stagingSiteURL>
					<inputEncoding>UTF-8</inputEncoding>
					<outputEncoding>UTF-8</outputEncoding>
				</configuration>

				<dependencies>

					<dependency>
						<groupId>org.apache.maven.doxia</groupId>
						<artifactId>doxia-site-renderer</artifactId>
						<version>1.7.1</version>
					</dependency>
					<dependency>
						<groupId>org.apache.maven.doxia</groupId>
						<artifactId>doxia-core</artifactId>
						<version>1.7</version>
					</dependency>
					<dependency>
						<groupId>org.apache.maven.wagon</groupId>
						<artifactId>wagon-ssh</artifactId>
						<version>2.10</version>
					</dependency>

					<dependency>
						<groupId>org.tinyjee.dim</groupId>
						<artifactId>doxia-include-macro</artifactId>
						<version>1.1</version>
					</dependency>
					<dependency>
						<groupId>org.apache.maven.doxia</groupId>
						<artifactId>doxia-module-markdown</artifactId>
						<version>1.7</version>
					</dependency>
				</dependencies>
			</plugin>

			<plugin>
				<groupId>org.tinyjee.dim</groupId>
				<artifactId>doxia-include-macro</artifactId>
				<version>1.1</version>
				<executions>
					<execution>
						<id>initialize-doxia-include-macro</id>
						<!-- Use "initialize" when the 'site' was not given as 'phase'. -->
						<!-- phase>initialize</phase -->
						<phase>pre-site</phase>
						<goals>
							<goal>initialize</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- Add Headless test engine, require open-jfx-monocle.jar deployed into jdk/jre/lib/ext -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.19.1</version>
				<configuration>
					<argLine>${argLine} -Dtestfx.robot=glass -Dglass.platform=Monocle -Dmonocle.platform=Headless -Dprism.order=sw</argLine>
				</configuration>
			</plugin>

			<!-- Code coverage -->
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.8</version>
				<executions>
					<execution>
						<id>default-prepare-agent</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>default-report</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

		</plugins>

		<extensions>
			<extension>
				<groupId>org.apache.maven.wagon</groupId>
				<artifactId>wagon-ssh</artifactId>
				<version>2.10</version>
			</extension>
		</extensions>

	</build>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.19.1</version>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.2.201409121644</version>
			</plugin>
		</plugins>
	</reporting>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit</artifactId>
				<version>4.12</version>
			</dependency>
			<dependency>
				<groupId>org.easytesting</groupId>
				<artifactId>fest-assert</artifactId>
				<version>1.4</version>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.easytesting</groupId>
			<artifactId>fest-assert</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<distributionManagement>
		<site>
			<id>JRebirth_Sites</id>
			<name>JRebirth Sites</name>
			<url>scp://s143909802.onlinehome.fr/kunden/homepages/14/d143909784/htdocs/jrebirth/sites/org.jrebirth.tooling-${project.version}</url>
		</site>
	</distributionManagement>

	<profiles>
		<!-- Override Bintray profile to use another Bintray Package -->
		<profile>
			<id>Bintray</id>
			<properties>
				<!-- Release repository -->
				<repositoryId>bintray-jrebirth-JRebirth-Tooling</repositoryId>
				<repositoryName>jrebirth-JRebirth-Tooling</repositoryName>
				<repositoryUrl>https://api.bintray.com/maven/jrebirth/JRebirth/Tooling/</repositoryUrl>
				<!-- Snapshot repository -->
				<snapshotRepositoryId>bintray-jrebirth-JRebirth-Tooling</snapshotRepositoryId>
				<snapshotRepositoryName>jrebirth-JRebirth-Tooling</snapshotRepositoryName>
				<snapshotRepositoryUrl>https://api.bintray.com/maven/jrebirth/JRebirth/Tooling/</snapshotRepositoryUrl>
			</properties>
		</profile>
	</profiles>


</project>
